'use strict';

var app = angular.module('dev',[]);


app.config(function($httpProvider){
    $httpProvider.defaults.headers.get = {
        pc: 'apic_live',
        Authorization:'Bearer eyJhbGciOiJIUzI1NiJ9.eyJwcm9qIjoiYXBpY19saXZlIiwic3ViIjoiMiIsImlzcyI6Imh0dHA6XC9cL2FwaWMuc21hcnRjdWJlMnUuY29tXC9hdXRoXC9sb2dpbiIsImlhdCI6IjE0NTEyODA1MzgiLCJleHAiOiIxNDUxMjkxMzM4IiwibmJmIjoiMTQ1MTI4MDUzOCIsImp0aSI6IjlhN2JhYzMyMDY1Y2Q5YWFiNWY3OTYzOTEzN2I0ZGEyIiwibGFuZyI6IkVOIiwiY2VudGVyIjoiTUFJTiIsImdycGlkIjoiQURNIiwiY29kZSI6InhpYW9xdWlrIiwibmFtZSI6ImhhbmRzb21lIHF1aWsiLCJwaWN0dXJlIjoiYXBwXC9pbWdcL3VzZXJcLzAyLmpwZyJ9.qQ7r_wYO8W4eWMnarB9HLeTAjjiSKsolh7WSIw3Y2gs'
    };

    //$httpProvider.defaults.withCredentials = true;
    //$httpProvider.defaults.crossDomain = true;
    //$httpProvider.defaults.headers.get = {
    //    pc: 'apic_live',
    //    token:'eyJhbGciOiJIUzI1NiJ9.eyJwcm9qIjoiYXBpY19saXZlIiwic3ViIjoiMiIsImlzcyI6Imh0dHA6XC9cL2FwaWMuc21hcnRjdWJlMnUuY29tXC9hdXRoXC9sb2dpbiIsImlhdCI6IjE0NTA5MjgxODciLCJleHAiOiIxNDUwOTM4OTg3IiwibmJmIjoiMTQ1MDkyODE4NyIsImp0aSI6Ijc0Zjc2ZGVlZmRiZWY2ZTViMGJlZDk2ZDI0YmEwNDUyIiwibGFuZyI6IkVOIiwiY2VudGVyIjoiTUFJTiIsImdycGlkIjoiQURNIiwiY29kZSI6InhpYW9xdWlrIiwibmFtZSI6ImhhbmRzb21lIHF1aWsiLCJwaWN0dXJlIjoiYXBwXC9pbWdcL3VzZXJcLzAyLmpwZyJ9.g5IMXjFunqsCt6IOSWTi6bsxwqjYaVj1KxFcSwFX3Ws'
    //};


});






app.controller('devCtrl', function($scope, $http,wallet) {


    $scope.listing = {};

    //->This is a resolve in the actual app
    wallet.getAll().then(function(data){
        $scope.listing = data;
    });

    $scope.getList = function(cparams){
        var params = {limit:$scope.listing.perPage};
        angular.extend(params,cparams);
        wallet.getAll(params).then(function(data){
            $scope.listing = data;
        });
    };

    //add comment
    $scope.headers = { i_owner_id:'ID',
        t_bank_acc_name:'Name',
        t_banker:'Bank',
        t_center_id: 'Center ID',
        t_type:'Type'
    };

    $scope.viewer = function(key){
        console.log('view--->',key);
    };

    $scope.editor = function(key){
        console.log('edit---->',key);
    };

    $scope.deleter = function(key){
        console.log('delete---->',key);
    };

    $scope.actions = [
                        {
                            label: 'View',
                            class: 'btn-success',
                            icon: 'glyphicon-eye-open',
                            fn :   $scope.viewer,
                            key : 'i_owner_id'
                        },
                        {
                            label: 'Edit',
                            class: 'btn-info',
                            icon: 'glyphicon-edit',
                            fn :   $scope.editor,
                            key : 'i_owner_id'
                        },
                        {
                            label: 'Remove',
                            class: 'btn-danger',
                            icon: 'glyphicon-remove',
                            fn :   $scope.deleter,
                            key : 'i_owner_id'
                        }
                    ];

});

app.factory('wallet', function ($http, $q) {
    return {
        getAll: function(cParams) {
            //var params = {};
            var params = {module:'wallettopup', action:''};
            angular.extend(params,cParams);
            return $http.get('http://demo.iscity.local/view',{params:params})
            //return $http.get('http://apic.smartcube2u.com/wallettopup',{params:params})
                .then(function(response){
                    return response.data;
                });
        }
    };
});


app.directive('mc2Datagrid', function () {
    return {
        restrict: "AE",
        replace: true,
        scope : {
            headers : '=',
            data : '=',
            resGetter : '=',
            limit : '=',
            actions : '='
        },
        templateUrl: 'mc2-datagrid.html',
        link : function(scope, element, attrs){


            scope.perPageOpt = [5,15,50,100,200,300,400,500];
            /*Default*/
            //scope.limit = 100;

            scope.objectKeys = function(obj){
                return Object.keys(obj);
            };

            function paginator(currentPage,totalPages){
                var pages = [];
                for(var i=1;i<=totalPages;i++) {
                    pages.push(i);
                }
                return pages;
            }

            function populate(data) {
                if(angular.isDefined(data.total)){
                    scope.total = data.total;
                    scope.lastPage = data.lastPage;
                    scope.currentPage = data.CurrentPage;
                    scope.range = paginator(data.currentPage,data.lastPage);
                    scope.perPage = parseInt(data.perPage);
                }
            }

            scope.$watch("data", function() {
                populate(scope.data);
            });

            populate(scope.data);

        }


    }
});
